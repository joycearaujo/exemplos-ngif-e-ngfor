import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretiva-ng-if',
  templateUrl: './diretiva-ng-if.component.html',
  styleUrls: ['./diretiva-ng-if.component.css']
})
export class DiretivaNgIfComponent implements OnInit {

  resposta1: boolean = false;
  resposta2: boolean = false;
  resposta3: boolean = false;
  resposta4: boolean = false;

  clicado1(){
    this.resposta1 = !this.resposta1;
  }

  clicado2(){
    this.resposta2 = !this.resposta2;
  }

  clicado3(){
    this.resposta3 = !this.resposta3;
  }

  clicado4(){
    this.resposta4 = !this.resposta4;
  }

  constructor() { }

  ngOnInit() {
  }

}
